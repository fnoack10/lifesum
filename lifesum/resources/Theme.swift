//
//  Theme.swift
//  lifesum
//
//  Created by Franco Noack on 23.03.20.
//  Copyright © 2020 Franco Noack. All rights reserved.
//

import UIKit

public class Theme: NSObject {

    enum Font: String {
        case light = "Avenir-Light"
        case lightOblique = "Avenir-LightOblique"
        case regular = "Avenir-Roman"
        case semibold = "Avenir-Medium"
        case bold = "Avenir-Heavy"
    }

}

extension UIColor {
    static let textColor = UIColor(hex: 0x6C6C6C)
    static let shadowColor = UIColor(hex: 0x000000)
    
    static let gradientRed = UIColor(hex: 0xED5460)
    static let gradientYellow = UIColor(hex: 0xF3A74E)
    
    convenience init(hex: Int, alpha: CGFloat = 1.0) {
        self.init(red: CGFloat((hex >> 16) & 0xff) / 255.0,
                  green: CGFloat((hex >> 8) & 0xff) / 255.0,
                  blue: CGFloat(hex & 0xff) / 255.0,
                  alpha: alpha)
    }
}

extension UIFont {
    convenience init!(_ font: Theme.Font, _ size: CGFloat) {
        self.init(name: font.rawValue, size: size)
    }
}

extension UILabel {
    func style(_ font: UIFont, _ textColor: UIColor) {
        self.font = font
        self.textColor = textColor
    }
}
