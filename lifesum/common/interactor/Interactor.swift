//
//  Interactor.swift
//  lifesum
//
//  Created by Franco Noack on 22.03.20.
//  Copyright © 2020 Franco Noack. All rights reserved.
//

import Foundation
import RxSwift

protocol InteractorInputs {}

protocol InteractorOutputs {
    var didFinish: PublishSubject<Void> { get }
}

protocol InteractorUIOutputs {}

protocol Interactor: InteractorInputs, InteractorOutputs, InteractorUIOutputs {
    var disposeBag: DisposeBag { get }
}
