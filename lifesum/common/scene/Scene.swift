//
//  Scene.swift
//  lifesum
//
//  Created by Franco Noack on 22.03.20.
//  Copyright © 2020 Franco Noack. All rights reserved.
//

import Foundation
import RxSwift

protocol Scene: class {
    var children: [Scene] { get set }
    var disposeBag: DisposeBag { get set }
    func start()
}

extension Scene {
    func addChild(_ scene: Scene) {
        if !children.contains(where: { $0 === scene }) {
            self.children.append(scene)
        }
    }
    
    func removeChild(_ scene: Scene) {
        self.children = self.children.filter { $0 !== scene }
    }
}
