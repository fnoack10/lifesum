//
//  APIError.swift
//  lifesum
//
//  Created by Franco Noack on 22.03.20.
//  Copyright © 2020 Franco Noack. All rights reserved.
//

import Foundation

enum APIError: Error {
    case parsingFailed
    case invalidResponse
    case requestFailed(String)
}
