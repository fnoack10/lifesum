//
//  APIResponse.swift
//  lifesum
//
//  Created by Franco Noack on 23.03.20.
//  Copyright © 2020 Franco Noack. All rights reserved.
//

import Foundation

struct APIResponse<T: Decodable>: Decodable {
    var meta: Metadata
    var response: T
}

struct Metadata: Decodable {
    var code: Int
}
