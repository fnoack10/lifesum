//
//  APIRouting.swift
//  lifesum
//
//  Created by Franco Noack on 22.03.20.
//  Copyright © 2020 Franco Noack. All rights reserved.
//

import Foundation

enum APIMethod: String {
    case get = "GET"
}

protocol APIRouting {
    var baseURL: String { get }
    var path: String { get }
    var method: APIMethod { get }
    var queryItems: [URLQueryItem] { get }
}

extension APIRouting {
    public var urlRequest: URLRequest {
        guard let  url = self.url else {
            fatalError("URL not found")
        }
        
        var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true)
        urlComponents?.queryItems = queryItems
        
        var request = URLRequest(url: urlComponents?.url ?? url)
        request.addValue(Config.authToken, forHTTPHeaderField: "Authorization")
        request.httpMethod = method.rawValue
        
        return request
    }
    
    private var url: URL? {
        var urlComponents = URLComponents(string: baseURL)
        urlComponents?.path = path
        return urlComponents?.url
    }
    
}
