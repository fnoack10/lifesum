//
//  AppScene.swift
//  lifesum
//
//  Created by Franco Noack on 22.03.20.
//  Copyright © 2020 Franco Noack. All rights reserved.
//

import Foundation
import RxSwift

final class AppScene: Scene {
    
    // MARK: Properties
    var didFinish =  PublishSubject<Void>()
    var children: [Scene] = []
    var disposeBag = DisposeBag()
    
    // MARK: Private Properties
    private let window: UIWindow
    private let api = APIGateway()
    private lazy var navigationController = self.createNavigationController()
    
    init(window: UIWindow) {
        self.window = window
        self.window.rootViewController = navigationController
    }
    
    func start() {
        launchFoodItemScene()
    }
    
    private func createNavigationController() -> UINavigationController {
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true
        return navigationController
    }
    
}

extension AppScene {
 
    private func launchFoodItemScene() {
        let foodItemScene = FoodItemScene(api: api, navigationController: navigationController)
        foodItemScene.start()
        addChild(foodItemScene)
    }

}
