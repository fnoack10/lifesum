//
//  FoodItemPM.swift
//  lifesum
//
//  Created by Franco Noack on 22.03.20.
//  Copyright © 2020 Franco Noack. All rights reserved.
//

import Foundation

struct FoodItemPM {
    var title: String = ""
    var caloriesValue: String = ""
    var caloriesTitle: String = ""
    var carbTitle: String = ""
    var carbsPercentage: String = ""
    var proteinTitle: String = ""
    var proteinPercentage: String = ""
    var fatTitle: String = ""
    var fatPercentage: String = ""
    
    init() {
        title = "SHAKE ME!"
    }
    
    init(_ item: FoodItem) {
        title = item.title.uppercased()
        caloriesTitle = "Calories per serving"
        carbTitle = "CARBS"
        proteinTitle = "PROTEIN"
        fatTitle = "FAT"
        caloriesValue = item.calories.description
        carbsPercentage = "\(Int(item.carbs))" + "%"
        proteinPercentage = "\(Int(item.protein))" + "%"
        fatPercentage = "\(Int(item.fat))" + "%"
    }
    
}
