//
//  FoodItemInteractor.swift
//  lifesum
//
//  Created by Franco Noack on 22.03.20.
//  Copyright © 2020 Franco Noack. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol FoodItemInteractorInputs: InteractorInputs {
    func bind(actions: FoodItemUI.Actions)
}

protocol FoodItemInteractorOutputs: InteractorOutputs {
}

protocol FoodItemInteractorUIOutputs: InteractorUIOutputs {
    var updateUI: Driver<FoodItemPM> { get }
}

protocol FoodItemInteractorType: Interactor {
    var inputs: FoodItemInteractorInputs { get }
    var outputs: FoodItemInteractorOutputs { get }
    var uiOutputs: FoodItemInteractorUIOutputs { get }
}

typealias FoodItemStream = Observable<FoodItemPM>

final class FoodItemInteractor: FoodItemInteractorType, FoodItemInteractorInputs, FoodItemInteractorOutputs, FoodItemInteractorUIOutputs {
    
    //  MARK: Inputs
    func configure(with presentationModel: FoodItemPM) {
        presentationModelSubject.accept(presentationModel)
    }
    
    //  MARK: Actions
    func bind(actions: FoodItemUI.Actions) {
        actions.shake.asObservable()
            .map { Int.random(in: 1 ... 200).description }
            .flatMap { self.loadFoodItem(id: $0) }
            .bind(to: presentationModelSubject)
            .disposed(by: disposeBag)
    }
    
    // MARK: Outputs
    var didFinish = PublishSubject<Void>()
    
    // MARK: UIOutputs
    lazy var updateUI: Driver<FoodItemPM> = self.createUpdateUIStream()

    // MARK: I/O
    var inputs: FoodItemInteractorInputs { return self }
    var outputs: FoodItemInteractorOutputs { return self }
    var uiOutputs: FoodItemInteractorUIOutputs { return self }
    
    // MARK: Properties
    let disposeBag = DisposeBag()
    
    //  MARK: Private Properties
    private let gateway: FoodGate
    private let presentationModelSubject = BehaviorRelay(value: FoodItemPM())
    
    init(gateway: FoodGate) {
        self.gateway = gateway
    }
    
    private func loadFoodItem(id: String) -> FoodItemStream {
        return gateway.loadFoodItem(id)
            .map { result in
                switch result {
                case .success(let item):
                    return FoodItemPM(item)
                case .failure(_):
                    return FoodItemPM()
                }
        }
    }
    
    // MARK: Helper Methods
    
    private func createUpdateUIStream() -> Driver<FoodItemPM> {
        return  presentationModelSubject.asDriver()
    }
    
}
