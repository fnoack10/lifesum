//
//  FoodItem.swift
//  lifesum
//
//  Created by Franco Noack on 22.03.20.
//  Copyright © 2020 Franco Noack. All rights reserved.
//

import Foundation

struct FoodItem: Decodable {
    var title: String
    var calories: Int
    var carbs: Float
    var protein: Float
    var fat: Float
    var saturatedfat: Float
    var unsaturatedfat: Float
    var fiber: Float
    var cholesterol: Float
    var sugar: Float
    var sodium: Float
    var potassium: Float
    var gramsperserving: Float
    var pcstext: String
}
