//
//  FoodRoute.swift
//  lifesum
//
//  Created by Franco Noack on 22.03.20.
//  Copyright © 2020 Franco Noack. All rights reserved.
//

import Foundation

enum FoodRouting {
    case loadFoodItem(id: String)
}

extension FoodRouting: APIRouting {
    
    var baseURL: String { return "https://api.lifesum.com" }
    var method: APIMethod { return .get }
    
    var path: String {
        switch self {
        case .loadFoodItem: return "/v2/foodipedia/codetest"
        }
    }
    
    var queryItems: [URLQueryItem] {
        switch self {
        case let .loadFoodItem(id): return [URLQueryItem(name: "foodid", value: id)]
        }
    }
    
}

