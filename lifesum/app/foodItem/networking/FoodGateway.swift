//
//  FoodGateway.swift
//  lifesum
//
//  Created by Franco Noack on 22.03.20.
//  Copyright © 2020 Franco Noack. All rights reserved.
//

import Foundation
import RxSwift

typealias FoodItemResultStream = Observable<Result<FoodItem, APIError>>

protocol FoodGate {
    func loadFoodItem(_ id: String) -> FoodItemResultStream
}

final class FoodGateway: FoodGate {
    
    private let api: APIGateway
    
    init(api: APIGateway) {
        self.api = api
    }
        
    func loadFoodItem(_ id: String) -> FoodItemResultStream {
        return Observable.create { observer in
            self.api.request(with: FoodRouting.loadFoodItem(id: id).urlRequest) { response in
                switch response {
                case .success(let data):
                    do {
                        let object = try JSONDecoder().decode(APIResponse<FoodItem>.self, from: data)
                        observer.onNext(.success(object.response))
                    } catch {
                        observer.onNext(.failure(.parsingFailed))
                    }
                case .failure(let error):
                    observer.onNext(.failure(error))
                }
            }
            return Disposables.create()
        }
    }
    
}
