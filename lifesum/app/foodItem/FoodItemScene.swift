//
//  FoodItemScene.swift
//  lifesum
//
//  Created by Franco Noack on 22.03.20.
//  Copyright © 2020 Franco Noack. All rights reserved.
//

import Foundation
import RxSwift

final class FoodItemScene: Scene {
    
    // MARK: Properties
    var didFinish = PublishSubject<Void>()
    var children: [Scene] = []
    var disposeBag = DisposeBag()
    
    // MARK: Private Properties
    private let api: APIGateway
    private let navigationController: UINavigationController
    private lazy var gateway: FoodGate = self.createFoodGateway()
    
    // MARK: - Lifecycle
    init(api: APIGateway, navigationController: UINavigationController) {
        self.api = api
        self.navigationController = navigationController
    }
    
    func start() {
        launchFoodItemUI()
    }
    
    private func createFoodGateway() -> FoodGate {
        return FoodGateway(api: api)
    }
    
}

// MARK: UI
extension FoodItemScene {
    
    private func launchFoodItemUI() {
        let interactor = FoodItemInteractor(gateway: gateway)
        let ui = FoodItemUI.create(with: interactor)
        interactor.inputs.bind(actions: ui.actions)
        
        navigationController.pushViewController(ui, animated: true)
    }
    
}
