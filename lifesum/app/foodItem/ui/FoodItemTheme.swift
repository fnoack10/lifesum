//
//  FoodItemTheme.swift
//  lifesum
//
//  Created by Franco Noack on 23.03.20.
//  Copyright © 2020 Franco Noack. All rights reserved.
//

import UIKit

struct FoodItemTheme {
    
    static func apply(circleView: UIView) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.gradientYellow.cgColor, UIColor.gradientRed.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.2, y: 0.2)
        gradientLayer.endPoint = CGPoint(x: 0.8, y: 0.8)
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = circleView.bounds
        
        circleView.layer.insertSublayer(gradientLayer, at: 0)
        circleView.layer.cornerRadius = circleView.frame.width/2
        circleView.clipsToBounds = true
    }
    
    static func apply(titleLabel: UILabel) {
        titleLabel.style(UIFont(.light, 20), .white)
    }
    
    static func apply(caloriesLabel: UILabel) {
        caloriesLabel.style(UIFont(.lightOblique, 20), .white)
    }
    
    static func apply(caloriesValueLabel: UILabel) {
        caloriesValueLabel.style(UIFont(.semibold, 60), .white)
    }
    
    static func apply(valueLabel: UILabel) {
        valueLabel.style(UIFont(.light, 18), .textColor)
    }
    
    static func apply(percentageLabel: UILabel) {
        percentageLabel.style(UIFont(.light, 18), .textColor)
        
    }

}
