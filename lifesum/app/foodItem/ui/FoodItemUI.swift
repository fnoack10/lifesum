//
//  FoodItemUI.swift
//  lifesum
//
//  Created by Franco Noack on 22.03.20.
//  Copyright © 2020 Franco Noack. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class FoodItemUI: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var caloriesLabel: UILabel!
    @IBOutlet weak var caloriesValueLabel: UILabel!
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var carbLabel: UILabel!
    @IBOutlet weak var carbValueLabel: UILabel!
    @IBOutlet weak var proteinLabel: UILabel!
    @IBOutlet weak var proteinValueLabel: UILabel!
    @IBOutlet weak var fatLabel: UILabel!
    @IBOutlet weak var fatValueLabel: UILabel!
    
    // MARK: Public Properties
    let disposeBag = DisposeBag()
    
    // MARK: Private Properties
    private var interactor: FoodItemInteractorType?
    private lazy var shakeSubject = PublishSubject<Void>()
    
    static func create(with interactor: FoodItemInteractorType) -> FoodItemUI {
        let storyboard = UIStoryboard(name: "FoodItem", bundle: nil)
        let ui = storyboard.instantiateViewController(withIdentifier: "FoodItemUI") as! FoodItemUI
        ui.interactor = interactor
        return ui
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()
        
        applyTheme()
        bindInteractor()
    }
    
    private func applyTheme() {
        FoodItemTheme.apply(titleLabel: titleLabel)
        FoodItemTheme.apply(caloriesLabel: caloriesLabel)
        FoodItemTheme.apply(caloriesValueLabel: caloriesValueLabel)
        FoodItemTheme.apply(circleView: circleView)
        FoodItemTheme.apply(valueLabel: carbLabel)
        FoodItemTheme.apply(percentageLabel: carbValueLabel)
        FoodItemTheme.apply(valueLabel: proteinLabel)
        FoodItemTheme.apply(percentageLabel: proteinValueLabel)
        FoodItemTheme.apply(valueLabel: fatLabel)
        FoodItemTheme.apply(percentageLabel: fatValueLabel)
    }
        
    private func bindInteractor() {
        guard let interactor = interactor else { return }
        interactor.uiOutputs.updateUI.drive(onNext: { [weak self] presentationModel in
            self?.updateUI(presentationModel)
        }).disposed(by: disposeBag)

    }
    
    private func updateUI(_ presentationModel: FoodItemPM) {
        titleLabel.text = presentationModel.title
        caloriesValueLabel.text = presentationModel.caloriesValue
        caloriesLabel.text = presentationModel.caloriesTitle
        carbLabel.text = presentationModel.carbTitle
        carbLabel.text = presentationModel.carbTitle
        proteinLabel.text = presentationModel.proteinTitle
        fatLabel.text = presentationModel.fatTitle
        carbValueLabel.text = presentationModel.carbsPercentage
        proteinValueLabel.text = presentationModel.proteinPercentage
        fatValueLabel.text = presentationModel.fatPercentage
    }
    
    // MARK: Motion Event
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?){
        if motion == .motionShake {
            shakeSubject.onNext(())
        }
    }
 
}

// MARK: Actions
extension FoodItemUI {
    struct Actions {
        let shake: Driver<Void>
    }
    
    var actions: Actions {
        return Actions(shake: shakeSubject.asDriver(onErrorJustReturn: ()))
    }
}
